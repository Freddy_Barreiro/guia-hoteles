$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();

    $('.carousel').carousel({
        interval: 2000
    });

    $('#contacto').on("show.bs.modal", function (e) {

        console.log(e);
        $("#contactoBtn").removeClass('btn btn-outline-danger');
        $("#contactoBtn").addClass('btn-primary');
    });

    $('#contacto').on("hide.bs.modal", function (e) {

        console.log(e);
        $("#contactoBtn").removeClass('btn-primary');
        $("#contactoBtn").addClass('btn btn-outline-danger');
        $("#contactoBtn").prop('disabled', true);

    });

});